import React from "react";

export default function ShoeItem(shoe) {
  return (
    <div className="col-3 mt-5">
      <div className="card text-white bg-secondary">
        <img className="card-img-top" src={shoe.data.image} alt />
        <div className="card-body">
          <h4 className="card-title">{shoe.data.name}</h4>
          <p className="card-title">{shoe.data.price}</p>
          <button
            onClick={() => {
              shoe.handleAddToCart(shoe.data);
            }}
            className="btn btn-info"
          >
            Add to cart
          </button>
        </div>
      </div>
    </div>
  );
}
