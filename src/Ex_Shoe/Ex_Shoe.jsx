import React, { useState } from "react";
import { data_shoes } from "./data_shoes";

import ShoeItem from "./ShoeItem";
import ShoeCart from "./ShoeCart";

export default function Ex_Shoe() {
  let [shoes, setShoes] = useState({
    listShoe: data_shoes,
    cart: [],
  });

  let renderContent = () => {
    return shoes.listShoe.map((item, index) => {
      return (
        <ShoeItem key={index} handleAddToCart={handleAddToCart} data={item} />
      );
    });
  };

  let handleAddToCart = (shoe) => {
    let index = shoes.cart.findIndex((item) => {
      return (item.id = shoe.id);
    });
    let cloneCart = [...shoes.cart];
    if (index == -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }
    setShoes({
      ...shoes,
      cart: cloneCart,
    });
  };

  let handleChangeQuantity = (shoeId, value) => {
    let index = shoes.cart.findIndex((item) => {
      return (item.id = shoeId);
    });

    if (index == -1) return;

    let cloneCart = [...shoes.cart];
    cloneCart[index].quantity = cloneCart[index].quantity + value;
    cloneCart[index].quantity == 0 && cloneCart.splice(index, 1);
    setShoes({
      ...shoes,
      cart: cloneCart,
    });
  };

  return (
    <div className="container py-5">
      <ShoeCart cart={shoes.cart} handleChangeQuantity={handleChangeQuantity} />
      <div className="row">{renderContent()}</div>
    </div>
  );
}
