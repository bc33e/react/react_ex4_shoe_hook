import React from "react";

export default function ShoeCart(shoe) {
  let renderTbody = () => {
    return shoe.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                shoe.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
            <span className="px-5">{item.quantity}</span>
            <button
              onClick={() => {
                shoe.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
          <td>{item.price * item.quantity}</td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead className="bg bg-secondary text-light">
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
